﻿using LadiesChoice.Models;
using LadiesChoice.Utilities;
using LadiesChoice.ViewModels;
using LadiesChoiceAdmin.ViewModels;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMatrix.WebData;

namespace LadiesChoiceAdmin.Controllers
{
    //[InitializeSimpleMembership]
    public class AdminHomeController : Controller
    {
        private string MenReEngagementHTMLTemplate = null;
        private string WomenReEngagementHTMLTemplate = null;
        private string ProfileCardReEngagementHTMLTemplate = null;
        private string MenReEngagementTextTemplate = null;
        private string WomenReEngagementTextTemplate = null;
        private string ProfileCardReEngagementTextTemplate = null;
        private string LogString = null;

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ViewFlaggedUsers(string city, string flashMessage = null)
        {
            DBEntities db = new DBEntities(city + "Connection");

            IEnumerable<UserProfile> flaggedProfiles = db.UserProfiles.Where(x=>x.IsSuspectedSpammer == true);
            ViewFlaggedUsersViewModel model = new ViewFlaggedUsersViewModel(flaggedProfiles, city);
            model.FlashMessage = flashMessage;

            return View(model);
        }

        public ActionResult ViewFlaggedMessages(string city, int userID, string flashMessage = null)
        {
            //get all messages that have been flagged as potential spam
            
            DBEntities db = new DBEntities(city + "Connection");
            UserProfile u = db.UserProfiles.FirstOrDefault(z => z.UserId == userID);
            IEnumerable<MessageModel> flaggedMessages = db.MessageModels.Where(x => x.SenderUserID == userID && x.NeedsSpamCheck == true);
            IEnumerable<MessageModel> allMessagesOfUser = db.MessageModels.Where(x => x.SenderUserID == userID || x.RecipientUserID == userID);

            ViewFlaggedMessagesViewModel model = new ViewFlaggedMessagesViewModel(flaggedMessages, allMessagesOfUser, city, u.UserName, userID);
            model.FlashMessage = flashMessage;

            return View(model);
        }

        public ActionResult ProfileDetailsByUsername(string city, string username) {
            DBEntities db = new DBEntities(city + "Connection");
            UserProfile u = db.UserProfiles.FirstOrDefault(x => x.UserName == username);
            if (u == null)
            {
                u = db.UserProfiles.FirstOrDefault(x => x.Email == username);
                if (u == null)
                {
                    Response.Write("Profile not found"); return null;
                }
            }
            ProfileDetailsViewModel model = new ProfileDetailsViewModel(db, u.UserId, city);
            return View("ProfileDetails", model);
        }

        public ActionResult ProfileDetails(string city, int userID, string flashMessage = null)
        {
            //get all messages that have been flagged as potential spam
            DBEntities db = new DBEntities(city + "Connection");
            ProfileDetailsViewModel model = new ProfileDetailsViewModel(db, userID, city);
            model.FlashMessage = flashMessage;
            return View(model);
        }

        public ActionResult ViewMessagesOfUser(string city, int userID)
        {
            DBEntities db = new DBEntities(city + "Connection");
            UserProfile u = db.UserProfiles.FirstOrDefault(z => z.UserId == userID);
            IEnumerable<MessageModel> flaggedMessages = db.MessageModels.Where(x => x.SenderUserID == userID && x.NeedsSpamCheck == true);
            IEnumerable<MessageModel> allMessagesOfUser = db.MessageModels.Where(x => x.SenderUserID == userID || x.RecipientUserID == userID).OrderBy(y => y.SentDate);

            ViewFlaggedMessagesViewModel model = new ViewFlaggedMessagesViewModel(flaggedMessages, allMessagesOfUser, city, u.UserName, userID);
            return View("ViewFlaggedMessages", model);
        }

        public ActionResult ApproveMessage(Guid MessageID, string city, int userID)
        {
            DBEntities db = new DBEntities(city + "Connection");
            UserProfile u = db.UserProfiles.FirstOrDefault(z => z.UserId == userID);
            MessageModel m = db.MessageModels.FirstOrDefault(x => x.MessageModelID == MessageID);
            m.NeedsSpamCheck = false;
            db.Entry(m).State = EntityState.Modified;
            if (u != null)
            {
                UserProfile recip = db.UserProfiles.FirstOrDefault(x => x.UserId == m.RecipientUserID);
                var subject = "New message from " + u.UserName + "!";
                
                var messageText = Properties.Settings.Default.NewMessageTemplate.Replace("<usernameOfSender>", u.UserName);
                messageText = messageText.Replace("<usernameOfRecipient>", recip.UserName);
                messageText = messageText.Replace("<ActiveCity>", city);
                

                SendMessageFromLadiesChoice(recip.Email,subject,messageText,city);


                db.SaveChanges();
                db.Dispose();

                return RedirectToAction("ViewFlaggedMessages", new { city = city, userID = userID, flashMessage = "message sent" });

            }

            db.Dispose();

            return RedirectToAction("ViewFlaggedMessages", new { city = city, userID = userID, flashMessage = "message send failed" });
        }

        public ActionResult MarkUserAsNonSpammer(string city, int userID)
        {
            try
            {
                DBEntities db = new DBEntities(city + "Connection");
                UserProfile u = db.UserProfiles.FirstOrDefault(z => z.UserId == userID);
                u.IsSuspectedSpammer = false;
                db.Entry(u).State = EntityState.Modified;
                db.SaveChanges();
                db.Dispose();
                return RedirectToAction("ViewFlaggedUsers", new { city = city, userID = userID, flashMessage = "marked user " + userID + " as non-spammer" });

            }
            catch (Exception e)
            {
                return RedirectToAction("ViewFlaggedUsers", new { city = city, userID = userID, flashMessage = "marked user " + userID + " failed.  Exception:"+ e.Message });
            }
        }

        public void ChangePasswordOfUser(string city, int userID, string newPassword) {
            DBEntities db = new DBEntities(city + "Connection");
            UserProfile u = db.UserProfiles.FirstOrDefault(z => z.UserId == userID);
            
            string resetToken = WebSecurity.GeneratePasswordResetToken(u.UserName);
            bool changePasswordSucceeded = WebSecurity.ResetPassword(resetToken, newPassword);
            if (changePasswordSucceeded)
            {
                Response.Write(u.UserName + "'s password changed to " + newPassword);
            }
            else
            {
                Response.Write("Error Password changing " + u.UserName + "'s password to " + newPassword + " didn't work.");

            }
        }

        public ActionResult DisableAccount(int id, string city)
        {
            DBEntities db = new DBEntities(city + "Connection");
            UserProfile model = db.UserProfiles.Find(id);
            model.IsDisabled = true;
            db.SaveChanges();
            db.Dispose();
            return RedirectToAction("ProfileDetails", new { userID = id, city = city, flashMessage = "account disabled." });
        }
        public ActionResult EnableAccount(int id, string city)
        {
            DBEntities db = new DBEntities(city + "Connection");
            UserProfile model = db.UserProfiles.Find(id);
            model.IsDisabled = false;
            db.SaveChanges();
            db.Dispose();
            return RedirectToAction("ProfileDetails", new { userID = id, city = city, flashMessage = "account enabled." });
        }

        public void DeleteUser(int userID, string city) {
            DBEntities db = new DBEntities(city + "Connection");
            UserProfile u = db.UserProfiles.Find(userID);
            if (u == null)
            {
                Response.Write("user not found");
                return;
            }
            //string username = u.UserName;
            //delete all of a users stuff
            var photos = db.PhotoModels.Where(x => x.UserID == userID);
            string thumbPhotoName;
            string mainPhotoName;
            foreach (PhotoModel photomodel in photos)
            {
                mainPhotoName = "main/" + photomodel.PhotoModelID + ".jpg";
                thumbPhotoName = "thumb/" + photomodel.PhotoModelID + ".jpg";
                AWSFunctions awsf = new AWSFunctions();
                awsf.DeleteFile(mainPhotoName);
                awsf.DeleteFile(thumbPhotoName);
                db.PhotoModels.Remove(photomodel);
            }
            var messages = db.MessageModels.Where(x => x.SenderUserID == userID);
            foreach (MessageModel m in messages)
            {
                m.Read = true; // in the case where the user sent a message to someone who hasn't read it (otherwise they'll have an unread message they can't read)
                db.Entry(m).State = EntityState.Modified;
            }
            MyPreferencesModel mpm = db.MyPreferencesModels.FirstOrDefault(y => y.UserID == userID);
            if (mpm != null) { db.MyPreferencesModels.Remove(mpm); }
            ProfileModel pm = db.ProfileModels.FirstOrDefault(z => z.iUserID == userID);
            if (pm != null) { db.ProfileModels.Remove(pm); }
            //UserProfile u = db.UserProfiles.FirstOrDefault(v => v.UserId == userID);
            u.IsDeleted = true;
            u.IsDisabled = true;
            string email = u.Email;
            u.Email = u.UserName + "deleted_user" + DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day + DateTime.Now.Second;
            u.UserName = u.UserName + "deleted_user" + DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day + DateTime.Now.Second;
            db.SaveChanges();
            db.Dispose();
            Response.Write("user deleted");
        }
        
        public void DeletePhoto(Guid id, string city)
        {
            DBEntities db = new DBEntities(city + "Connection");

            PhotoModel photomodel = db.PhotoModels.Find(id);
            if (photomodel.IsMainPhoto == true)
            {
                PhotoModel p = db.PhotoModels.FirstOrDefault(x => x.UserID == photomodel.UserID && x.PhotoModelID != id);
                if (p != null)
                {
                    p.IsMainPhoto = true;
                }
            }
            db.PhotoModels.Remove(photomodel);
            db.SaveChanges();
            db.Dispose();

            string mainPhotoName = "main/" + id + ".jpg";
            string thumbPhotoName = "thumb/" + id + ".jpg";

            AWSFunctions awsf = new AWSFunctions();
            awsf.DeleteFile(mainPhotoName);
            awsf.DeleteFile(thumbPhotoName);
            Response.Write("Photo: " + mainPhotoName + " deleted");
        }

        private string GetReEngagementTemplate(string templateName)
        {
            
            FileInfo fi = new FileInfo(System.Web.HttpContext.Current.Server.MapPath("/Content/"+templateName));
            StreamReader reader = fi.OpenText();
            string line;
            string strTemplate = null;
            while ((line = reader.ReadLine()) != null)
            {
                strTemplate += line;
            }
            reader.Close();

            return strTemplate;
        }

        public ActionResult RunReEngagementVancouver(bool isRunningInTest = false, int userIDtoStartAt = 0)
        {
            if (isRunningInTest)
            {
                RunReEngagement("Vancouver", "vancouver.com", "https://s3-us-west-2.amazonaws.com/ladieschoicetestbucket/", userIDtoStartAt);
            }
            else {
                RunReEngagement("Vancouver", "vancouver.com", "https://s3-us-west-2.amazonaws.com/ladieschoicevancouver/", userIDtoStartAt);
            }
            Response.Write(LogString + "<br><br>ReEnagement Script complete");
            return View("ReEngagementResults");
        }

        public void RunReEngagement(string city, string localDomain, string imagePath, int userIDtoStartAt = 0) {
            LogString = "";
            MenReEngagementHTMLTemplate = GetReEngagementTemplate("MenReEngagementTemplate.html");
            WomenReEngagementHTMLTemplate = GetReEngagementTemplate("WomenReEngagementTemplate.html");
            ProfileCardReEngagementHTMLTemplate = GetReEngagementTemplate("ProfileCardReEngagementTemplate.html");
            MenReEngagementTextTemplate = GetReEngagementTemplate("MenReEngagementTextTemplate.txt").Replace("<br>", "\r\n");
            WomenReEngagementTextTemplate = GetReEngagementTemplate("WomenReEngagementTextTemplate.txt").Replace("<br>", "\r\n");
            ProfileCardReEngagementTextTemplate = GetReEngagementTemplate("ProfileCardReEngagementTextTemplate.txt").Replace("<br>", "\r\n");
            DBEntities db = new DBEntities(city + "Connection");
            DateTime LastEngagementCutoffDate = DateTime.Now.AddDays(-2);
            IEnumerable<UserProfile> activeUsersOnTheSite = db.UserProfiles.Where(z => z.IsDisabled == false && z.IsDeleted == false && z.SurpressReEngagementEmails == false && z.IsSuspectedSpammer == false && z.HasCompletedProfile == true && z.LastReEngagementEmailDate < LastEngagementCutoffDate && z.LastLoginDate < LastEngagementCutoffDate && z.UserId >= userIDtoStartAt).Take(100);
            foreach (UserProfile u in activeUsersOnTheSite)
            {
                LogString = LogString + "<br>re-engaging " + u.UserName + " (userid:" + u.UserId + ")";
                RunReEnagementOnUser(u, city, localDomain, 3, imagePath, null);
            }
            db.Dispose();

        }

        private void RunReEnagementOnUser(UserProfile u, string city, string localDomain, int limitMatches, string imagePath, DBEntities db)
        {
            bool isLocalConnection = false;
            if (db == null)
            {
                isLocalConnection = true;
                db = new DBEntities(city + "Connection");
            }
            string emailBodyText = "";
            string emailBodyHTML = "";
            if (u != null)
            {
                DateTime lastEngagementDate;
                if (u.LastLoginDate > u.LastReEngagementEmailDate)
                {
                    lastEngagementDate = u.LastLoginDate;
                }
                else {
                    lastEngagementDate = u.LastReEngagementEmailDate;
                }
                ProfileModel pr = db.ProfileModels.FirstOrDefault(y => y.iUserID == u.UserId);
                bool isFemale = false;
                if (pr.Gender == Enums.Gender.Female.GetHashCode()) { 
                    isFemale = true;
                    emailBodyHTML = WomenReEngagementHTMLTemplate;
                    emailBodyText = WomenReEngagementTextTemplate;
                }
                else {
                    emailBodyHTML = MenReEngagementHTMLTemplate;
                    emailBodyText = MenReEngagementTextTemplate;
                }
                IEnumerable<ProfileCardViewModel> matches = MatchesSinceLastEngagementDate(db, u.UserId, isFemale, lastEngagementDate, limitMatches);
                //string matchesText = "";
                if (matches != null) {
                    int numMatches = matches.Count();
                    if (numMatches > 1)
                    {
                        //foreach (ProfileCardViewModel match in matches)
                        //{
                        //    string profileCardText = ProfileCardReEngagementTemplate;
                        //    profileCardText = profileCardText.Replace("{{Username}}", match.Name);
                        //    profileCardText = profileCardText.Replace("{{Age}}", match.Age.ToString());
                        //    profileCardText = profileCardText.Replace("{{AreaOfTown}}", match.Area.ToString());
                        //    profileCardText = profileCardText.Replace("{{Image}}", imagePath + "thumb/" + match.PhotoModelID.ToString() + ".jpg");


                        //    matchesText += profileCardText;
                        //}
                        //emailBodyText = emailBodyText.Replace("{{NumberMatches}}", numMatches.ToString());
                        //emailBodyText = emailBodyText.Replace("{{matches}}", matchesText);
                        //emailBodyText = emailBodyText.Replace("{{Username}}", u.UserName);
                        //emailBodyText = emailBodyText.Replace("{{LocalDomain}}", localDomain);
                        emailBodyHTML = ReplaceReEngagementEmailMergeFields(emailBodyHTML, ProfileCardReEngagementHTMLTemplate, matches, u.UserName, localDomain, imagePath, isFemale);

                        emailBodyText = ReplaceReEngagementEmailMergeFields(emailBodyText, ProfileCardReEngagementTextTemplate, matches, u.UserName, localDomain, imagePath, isFemale);
                        LogString = LogString + emailBodyHTML;

                        //SendMessageFromLadiesChoice(u.UserName + "@mailinator.com", "New Matches!", emailBodyText, city);

                        SendMessageFromLadiesChoice(u.Email, "New Matches!", emailBodyText, city, emailBodyHTML);
                        
                        u.LastReEngagementEmailDate = DateTime.Now;
                        db.Entry(u).State = EntityState.Modified;

                        db.SaveChanges();
                    }
                    //one or less match so don't send this user an email
                }

            }
            if (isLocalConnection){
                db.Dispose();
            }
            return;
        }

        private string ReplaceReEngagementEmailMergeFields(string templateText, string profileCardTemplate, IEnumerable<ProfileCardViewModel> matches, string username, string localDomain, string imagePath, bool isFemale) 
        {
            if (isFemale)
            {
                profileCardTemplate = profileCardTemplate.Replace("{{GenderPossessive}}", "his");
            }
            else {
                profileCardTemplate = profileCardTemplate.Replace("{{GenderPossessive}}", "her");
            }
            string matchesText = "";
            foreach (ProfileCardViewModel match in matches)
                        {
                            string profileCardText = profileCardTemplate;
                            profileCardText = profileCardText.Replace("{{Username}}", match.Name);
                            profileCardText = profileCardText.Replace("{{Age}}", match.Age.ToString());
                            profileCardText = profileCardText.Replace("{{AreaOfTown}}", match.Area.ToString());
                            profileCardText = profileCardText.Replace("{{Image}}", imagePath + "thumb/" + match.PhotoModelID.ToString() + ".jpg");
                            profileCardText = profileCardText.Replace("{{ProfileModelID}}", match.ProfileModelID.ToString());
                            LogString = LogString + "<br>&nbsp;&nbsp;" + match.Name + " (userid:" + match.MatchUserID + ")";

                            matchesText += profileCardText;
                        }
            templateText = templateText.Replace("{{NumberMatches}}", matches.Count().ToString());
            templateText = templateText.Replace("{{matches}}", matchesText);
            templateText = templateText.Replace("{{Username}}", username);
            templateText = templateText.Replace("{{LocalDomain}}", localDomain);
            return templateText;
        }

        private IEnumerable<ProfileCardViewModel> MatchesSinceLastEngagementDate(DBEntities db, int userID, bool isFemale, DateTime lastEngagementDate, int limitMatches)
        { 
            MyPreferencesModel p = db.MyPreferencesModels.FirstOrDefault(x => x.UserID == userID);
            if (p != null)
            {
                return LadiesChoice.Utilities.MyMatchesSupport.getMyMutualMatchProfiles(db, p, userID, isFemale, limitMatches, lastEngagementDate);
            }
            return null;
        }

        public ActionResult ViewAllWomen(string city)
        {
                        DBEntities db = new DBEntities(city + "Connection");

                        IEnumerable<ProfileCardViewModel> p = from pf in db.ProfileModels
                           join ph in db.PhotoModels on pf.iUserID equals ph.UserID
                           join ur in db.UserProfiles on pf.iUserID equals ur.UserId
                           where pf.Gender == 1 && ph.IsMainPhoto == true && ur.UserId > 30 && ur.IsSuspectedSpammer == false
                                                        select new ProfileCardViewModel()
                                                        {
                                                            ProfileModelID = pf.ProfileModelID,
                                                            PhotoModelID = ph.PhotoModelID,
                                                            Area = null,
                                                            Height = pf.Height,
                                                            Name = ur.UserName,
                                                            profile = true,
                                                            Age = 0,
                                                            BirthDate = pf.BirthDate,
                                                            LastLoginUTC = ur.LastLoginDate,
                                                            MatchUserID = ur.UserId
                                                        };
            return View(p);     
        }
        private static IRestResponse SendMessageFromLadiesChoice(string to, string subject, string text, string city, string html = null)
        {
            
            //text = text + to;
            //to = "pripley@gmail.com";
            RestClient client = new RestClient();
            client.BaseUrl = "https://api.mailgun.net/v2";
            client.Authenticator =
                    new HttpBasicAuthenticator("api",
                                               "key-14344fg7o-6xgzkyogcp7iyposse4qn8");
            RestRequest request = new RestRequest();
            if (city.Equals("Vancouver"))
            {
                request.AddParameter("domain",
                                 "ladieschoicevancouver.com", ParameterType.UrlSegment);

                request.AddParameter("from", "LadiesChoiceVancouver <noreply@ladieschoicevancouver.com>");
                text = text.Replace("<LocalDomain>", "vancouver.com");
            }
            else
            {
                throw new Exception("no city specified");
            }
            
            request.Resource = "{domain}/messages";
            request.AddParameter("to", to);
            request.AddParameter("subject", subject);
            request.AddParameter("text", text);
            if (html != null)
            {
                request.AddParameter("html", html);
            }
            request.Method = Method.POST;
            return client.Execute(request);
        }
    }
}
