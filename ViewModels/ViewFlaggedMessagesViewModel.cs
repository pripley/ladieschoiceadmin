﻿using LadiesChoice.Models;
using LadiesChoice.Utilities;
using LadiesChoice.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LadiesChoiceAdmin.ViewModels
{
    public class ViewFlaggedMessagesViewModel
    {

        public IEnumerable<MessageModel> FlaggedMessages { get; set; }
        public IEnumerable<MessageModel> AllMessages { get; set; }

        //public string ThumbPhotoPath { get; set; }
        //public string UsernameOfConversationPartner { get; set; }
        //public int UserIDOfConversationPartner { get; set; }
        public string City;
        public string Username;
        public int UserID;
        public string FlashMessage;
        public ViewFlaggedMessagesViewModel(IEnumerable<MessageModel> flaggedMessages, IEnumerable<MessageModel> allMessages, string city, string username, int userID)
        {
            FlaggedMessages = flaggedMessages;
            AllMessages = allMessages;
            City = city;
            Username = username;
            UserID = userID;
            //var flaggedMessages = from ur in db.UserProfiles
            //                   join m in db.MessageModels on ur.UserId equals m.SenderUserID
            //                   join p in db.ProfileModels on ur.UserId equals p.iUserID into pjoined
            //                   from pj in pjoined.DefaultIfEmpty()
            //                   join ph in db.PhotoModels on ur.UserId equals ph.UserID into joined
            //                   from j in joined.DefaultIfEmpty()
            //                   where m.NeedsSpamCheck == true && (j.IsMainPhoto == true || j.IsMainPhoto == null)
            //                   && ur.UserId == userID
            //                   orderby m.SentDate ascending
            //                   select new IndividualMessageViewModel()
            //               {
            //                   SentDate = m.SentDate,
            //                   PhotoModelID = j.PhotoModelID,
            //                   UsernameOfMessageSender = ur.UserName,
            //                   UserIDOfMessageSender = ur.UserId,
            //                   MessageText = m.MessageText,
            //                   Read = m.Read,
            //                   ProfileModelID = pj.ProfileModelID,
            //                   MessageModelID = m.MessageModelID
            //               };

            //this.Messages = flaggedMessages.ToList<IndividualMessageViewModel>();
            //this.ThumbPhotoPath = ""; 
            // ConfigurationManager.AppSettings.Get("PhotoPath") + "thumb/";
            //UserProfile u = db.UserProfiles.FirstOrDefault(x => x.UserId == userIDOfConversationPartner);
            //this.UsernameOfConversationPartner = u.UserName;
            //this.UserIDOfConversationPartner = u.UserId;
        }
    }
}