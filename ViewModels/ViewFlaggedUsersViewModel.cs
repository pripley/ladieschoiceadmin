﻿using LadiesChoice.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LadiesChoiceAdmin.ViewModels
{
    public class ViewFlaggedUsersViewModel
    {
        public IEnumerable<UserProfile> FlaggedProfiles;
        public string City;
        public string FlashMessage;
        public ViewFlaggedUsersViewModel(IEnumerable<UserProfile> flaggedProfiles, string city) {
            FlaggedProfiles = flaggedProfiles;
            City = city;
        }
    }
}