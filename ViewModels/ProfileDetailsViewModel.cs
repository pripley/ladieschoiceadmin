﻿using LadiesChoice.Models;
using LadiesChoice.Utilities;
using LadiesChoice.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LadiesChoiceAdmin.ViewModels
{
    public class ProfileDetailsViewModel : CommonAdminViewModel
    {
        public string City { get; set; }
        public Guid ProfileModelID { get; set; }
        public int UserIDOfProfile { get; set; }
        public string IPAddress { get; set; }
        public string UsernameOfProfile { get; set; }
        public string area { get; set; }
        public string Job { get; set; }
        public string Smokes { get; set; }
        public string Drinks { get; set; }
        public string LookingFor { get; set; }
        public string Education { get; set; }
        public string Age { get; set; }
        public string Description { get; set; }
        public string Hobbies { get; set; }
        public string ValueInAPartner { get; set; }
        public string TVShows { get; set; }
        public string ExtraQuestion1 { get; set; }
        public string WhatILikeAboutMyCity { get; set; }

        public string Height { get; set; }
        public string BodyType { get; set; }
        public string Religion { get; set; }
        public string HaveChildren { get; set; }
        public string WantChildren { get; set; }
        public string Income { get; set; }
        public bool HasUserDisabledTheirAccount { get; set; }
        public int Gender { get; set; }
        public int NumberOfFlaggedMessages { get; set; }
        public string PhotoPath { get; set; }
        public Guid? PhotoModelID { get; set; }
        public IEnumerable<PhotoModel> Photos { get; set; }
        public int TotalMessagesSent { get; set; }
        public int TotalMessagesReceived { get; set; }
        public string Email { get; set; }

        public ProfileDetailsViewModel(DBEntities db, int userID, string city) {
            City = city;
            if (city.Equals("Vancouver")) {
                PhotoPath = "https://s3-us-west-2.amazonaws.com/ladieschoicevancouver/";
            }
            else
            {
                throw new Exception("no city specified");
            }
            UserProfile u = db.UserProfiles.FirstOrDefault(x => x.UserId == userID);
            Email = u.Email;
            ProfileModel p = db.ProfileModels.FirstOrDefault(x => x.iUserID == userID);
            Photos = db.PhotoModels.Where(y => y.UserID == userID);

            NumberOfFlaggedMessages = db.MessageModels.Count(x => x.SenderUserID == userID && x.NeedsSpamCheck == true);

                      this.HasUserDisabledTheirAccount = u.IsDisabled;
                      this.UserIDOfProfile = userID;
                      this.ProfileModelID = p.ProfileModelID;
                      this.Job = p.Job;
                      IPAddress = u.LastIPAddress;
                      //this.area = p.areaID;
                      this.Gender = p.Gender;
                      this.UsernameOfProfile = u.UserName;
                      if (p.Description != null) { this.Description = p.Description.Replace("\n", "<br>"); }
                      if (p.Hobbies != null) { this.Hobbies = p.Hobbies.Replace("\n", "<br>"); }
                      if (p.ValueInAPartner != null) { this.ValueInAPartner = p.ValueInAPartner.Replace("\n", "<br>"); }
                      if (p.TVShows != null) { this.TVShows = p.TVShows.Replace("\n", "<br>"); }
                      if (p.ExtraQuestion1 != null) { this.ExtraQuestion1 = p.ExtraQuestion1.Replace("\n", "<br>"); }
                      if (p.WhatILikeAboutMyCity != null) { this.WhatILikeAboutMyCity = p.WhatILikeAboutMyCity.Replace("\n", "<br>"); }


                      this.Education = EnumExtensions.EnumValueToDisplayString<Enums.EducationLevel>(p.Education);
                      this.Drinks = EnumExtensions.EnumValueToDisplayString<Enums.Drinks>(p.Drinks);
                      this.Smokes = EnumExtensions.EnumValueToDisplayString<Enums.Smokes>(p.Smokes);
                      this.Income = EnumExtensions.EnumValueToDisplayString<Enums.Income>(p.Income);
                      this.Height = EnumExtensions.EnumValueToDisplayString<Enums.Height>(p.Height);
                      this.Religion = EnumExtensions.EnumValueToDisplayString<Enums.Religion>(p.Religion);
                      this.Age = AgeHelper.getAgeToday(p.BirthDate).ToString();
                      this.LookingFor = EnumExtensions.EnumMultiValuesToDisplayString<Enums.LookingFor>(p.LookingFor);
                      this.HaveChildren = EnumExtensions.EnumValueToDisplayString<Enums.HaveChildren>(p.HaveChildren);
                      this.WantChildren = EnumExtensions.EnumValueToDisplayString<Enums.WantChildren>(p.WantChildren);

                      TotalMessagesReceived = db.MessageModels.Count(x => x.RecipientUserID == u.UserId) - 1;
                      TotalMessagesSent = db.MessageModels.Count(x => x.SenderUserID == u.UserId);

                  }
    }
}